﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class Company
    {
        public long CompanyId { get; set; }

        public string Cnpj { get; set; }

        public long UserId { get; set; }

        public string CompanyName { get; set; }

        public string CorporateName { get; set; }

        public DateTime FoundationDate { get; set; }

        public decimal ShareCapital { get; set; }

        public string Industry { get; set; }

        public string ContactName { get; set; }

        public string Cpf { get; set; }

        public string Phone { get; set; }

        public string Mail { get; set; }

        public int? QuantityVehicules { get; set; }

        //public TypeVehiculeEnum? TypeVehicule { get; set; }

        public Address Address { get; set; }

        public string CarRental { get; set; }

        public int? IdUserCreated { get; set; }

        public int? IdUserUpdated { get; set; }

        public bool? IsDeleted { get; set; }

        //public CompanyTypeEnum? CompanyType { get; set; }

        //public ICollection<ContactEntity> Contacts { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}
