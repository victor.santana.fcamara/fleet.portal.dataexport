﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class Stock
    {
        public long StockId { get; set; }

        public int Quantity { get; set; }

        public string ModelColor { get; set; }
    }
}
