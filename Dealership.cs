﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class Dealership
    {
        public long ID { get; set; }

        public long DealershipGroupID { get; set; }

        public long Number { get; set; }

        public string Street { get; set; }

        public string AddressNumber { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string CNPJ { get; set; }

        public bool Status { get; set; }

        public string Domains { get; set; }

        public DateTime CreateDate { get; set; }

        public string Segments { get; set; }

        public string Uf { get; set; }

        public string City { get; set; }
    }
}
