﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class OrderContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrdersItems> OrdersItems { get; set; }

        public OrderContext(DbContextOptions<OrderContext> options) :
            base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>().HasKey(c => c.OrderId);
            modelBuilder.Entity<OrderHistoryEntity>().HasKey(c => c.OrderHistoryId);
            modelBuilder.Entity<OrderStatusEntity>().HasKey(c => c.OrderStatusId);
            modelBuilder.Entity<OrdersItems>().HasKey(c => c.OrderItemId);
            modelBuilder.Entity<OrderItemStatusEntity>().HasKey(c => c.OrderItemStatusId);
        }
    }
}
