﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class CustomerContext : DbContext
    {
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Job> Job { get; set; }
        public DbSet<Spouse> Spouse { get; set; }

        public CustomerContext(DbContextOptions<CustomerContext> options) :
            base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasKey(c => c.CustomerId);
            modelBuilder.Entity<Address>().HasKey(c => c.AddressId);
            modelBuilder.Entity<Job>().HasKey(c => c.JobId);
            modelBuilder.Entity<Spouse>().HasKey(c => c.SpouseId);
        }
    }
   
}
