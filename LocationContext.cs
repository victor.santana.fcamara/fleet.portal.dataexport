﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class LocationContext : DbContext
    {
        public DbSet<Location> Location { get; set; }

        public LocationContext(DbContextOptions<LocationContext> options) :
            base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Location>().HasKey(c => c.Id);
        }
    }
   
}
