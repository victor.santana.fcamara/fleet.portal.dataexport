﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class Order
    {
        public long OrderId { get; set; }

        public long UserId { get; set; }

        public long LocationId { get; set; }

        public long Score { get; set; }

        public long? UserIdDn { get; set; }

        public long? DealershipId { get; set; }

        public long? DealershipGroupId { get; set; }

        public OrderStatusEnum? Status { get; set; }

        public string Fullname { get; set; }

        public string CpfCnpj { get; set; }

        public string Note { get; set; }

        public string DocusignId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        public ICollection<OrderStatusEntity> OrderStatus { get; set; }

        public ICollection<OrderHistoryEntity> OrderHistory { get; set; }

        public ICollection<OrdersItems> OrderItem { get; set; }


    }

    public enum OrderStatusEnum
    {
        [Description("Pedido Cancelado")]
        OrderCanceled = 0,

        [Description("Pedido Criado")]
        OrderCreated = 1,

        [Description("Aguardando análise de antifraude")]
        AwaitingAntiFraudAnalysis = 2,

        [Description("Análise antifraude aprovada")]
        AntiFraudApproved = 3,

        [Description("Análise antifraude rejeitada")]
        AntiFraudRejected = 4,

        [Description("Aguardando análise de crédito")]
        AwaitingCreditAnalysis = 5,

        [Description("Análise de crédito aprovada")]
        CreditApproved = 6,

        [Description("Análise de crédito reprovada")]
        CreditRejected = 7,

        [Description("Retentativa de análise de crédito")]
        RetryCreditAnalysis = 8,

        [Description("Aguardando assinatura de contrato")]
        AwaitingContractSigning = 9,

        [Description("Contrato assinado")]
        SigningContract = 10,

        [Description("Pedido concluído")]
        OrderDone = 11,

        [Description("Aguardando autenticidade dos documentos")]
        AguardandoAutenticidadeDosDocumentos = 12,

        [Description("Autenticidade aprovada")]
        AutenticidadeAprovada = 13,

        [Description("Autenticidade reprovada")]
        AutenticidadeReprovada = 14,

        [Description("Aguardando envio de contrato")]
        AwaitingSendingOfContract = 15,

        [Description("Pendente Signatários")]
        AwaitingContactRegistration = 16,

        [Description("Aguardando logistica")]
        AwaitingLogistics = 17
    }

    public class OrderHistoryEntity
    {
        public long OrderHistoryId { get; set; }

        public long OrderId { get; set; }
        public Order Order { get; set; }

        public long UserId { get; set; }

        public long ProductId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }
    }

    public class OrderStatusEntity
    {
        public long OrderStatusId { get; set; }

        public Order Order { get; set; }
        public long OrderId { get; set; }

        public OrderStatusEnum Status { get; set; }

        public string Description { get; set; }

        public string Note { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }


    }

    public class OrdersItems
    {
        public long OrderItemId { get; set; }

        public OrderItemStatusEnum Status { get; set; }

        public Order Order { get; set; }
        public long OrderId { get; set; }

        public long ProductId { get; set; }

        public string Model { get; set; }

        public string Color { get; set; }

        public string TypeOfPainting { get; set; }

        public decimal MonthlyKmValue { get; set; }

        public decimal MonthlyInstallment { get; set; }

        public decimal OverrunKm { get; set; }

        public decimal HullFranchiseValue { get; set; }

        public int Deadline { get; set; }

        public string CityForPickup { get; set; }

        public string Uf { get; set; }

        public string Optional { get; set; }

        public string ModelCode { get; set; }


        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        public ICollection<OrderItemStatusEntity> OrderItemStatus { get; set; }
    }

    public class OrderItemStatusEntity
    {
        public long OrderItemStatusId { get; set; }

        public OrdersItems OrderItem { get; set; }
        public long OrderItemId { get; set; }

        public OrderItemStatusEnum Status { get; set; }

        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }
    }

    public enum OrderItemStatusEnum
    {
        [Description("Veículo Cancelado")]
        VehicleCancelled,

        [Description("Aguardando Faturamento")]
        AwaitingBilling,

        [Description("Aguardando Preparação")]
        AwaitingPreparation,

        [Description("Aguardando Documentação")]
        AwaitingDocumentation,

        [Description("Aguardando Transporte")]
        AwaitingTransport,

        [Description("Veículo Entregue")]
        VehicleDelivered
    }

}
