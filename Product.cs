﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class Product 
    {
        public long ProductId { get; set; }

        public string Model { get; set; }

        public string Color { get; set; }

        public long ModelCode { get; set; }

        public string PlanCode { get; set; }
    }
}
