﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using fleet.portal.dataexport.Models;
using System.Text;
using fleet.portal.dataexport;
using ClosedXML.Excel;
using System.IO;

namespace fleet.portal.dataexport.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Export([FromServices] UserContext userContext, [FromServices] OrderContext orderContext, [FromServices] ProductContext productContext, [FromServices] LocationContext locationContext, [FromServices] CustomerContext customerContext, [FromServices] DealershipContext dealershipContext)
        {
            var username = Request.Form["username"];
            var password = Request.Form["pwd"];

            if (username != "fleetexport" || password != "1234!@#$fleet")
            {
                return RedirectToAction("Index");
            }

            var users = userContext.User.OrderBy(p => p.UserId).ToList();
            Console.WriteLine($"{username} / {password}");
            var orders = orderContext.Orders.OrderBy(p => p.OrderId).ToList();
            var orderItens = orderContext.OrdersItems.OrderBy(p => p.OrderId).ToList();
            var products = productContext.Products.ToList();
            var locations = locationContext.Location.ToList();
            var customers = customerContext.Customer.ToList();
            var stock = productContext.Stock.ToList();
            var address = customerContext.Address.ToList();
            var job = customerContext.Job.ToList();
            var spouse = customerContext.Spouse.ToList();
            var dealership = dealershipContext.Dealership.ToList();

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Usuários e Pedidos");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "ID USUÁRIO";
                worksheet.Cell(currentRow, 2).Value = "NOME";
                worksheet.Cell(currentRow, 3).Value = "CPF";
                worksheet.Cell(currentRow, 4).Value = "RG";
                worksheet.Cell(currentRow, 5).Value = "TELEFONE";
                worksheet.Cell(currentRow, 6).Value = "CELULAR";
                worksheet.Cell(currentRow, 7).Value = "E-MAIL";
                worksheet.Cell(currentRow, 8).Value = "E-MAIL ALTERNATIVO";
                worksheet.Cell(currentRow, 9).Value = "DATA DE NASCIMENTO";
                worksheet.Cell(currentRow, 10).Value = "GENERO";
                worksheet.Cell(currentRow, 11).Value = "ESTADO CIVIL";
                worksheet.Cell(currentRow, 12).Value = "EMPRESA";
                worksheet.Cell(currentRow, 13).Value = "DATA DE ADMISSÃO";
                worksheet.Cell(currentRow, 14).Value = "TELEFONE";
                worksheet.Cell(currentRow, 15).Value = "CARGO";
                worksheet.Cell(currentRow, 16).Value = "SALÁRIO";
                worksheet.Cell(currentRow, 17).Value = "NOME CÔNJUGE";
                worksheet.Cell(currentRow, 18).Value = "CPF CONJUGE";
                worksheet.Cell(currentRow, 19).Value = "SALARIO CONJUGE";
                worksheet.Cell(currentRow, 20).Value = "TELEFONE";
                worksheet.Cell(currentRow, 21).Value = "E-MAIL CONJUGE";
                worksheet.Cell(currentRow, 22).Value = "CEP";
                worksheet.Cell(currentRow, 23).Value = "LOGRADOURO";
                worksheet.Cell(currentRow, 24).Value = "NUMERO";
                worksheet.Cell(currentRow, 25).Value = "COMPLEMENTO";
                worksheet.Cell(currentRow, 26).Value = "BAIRRO";
                worksheet.Cell(currentRow, 27).Value = "CIDADE";
                worksheet.Cell(currentRow, 28).Value = "ESTADO";
                worksheet.Cell(currentRow, 29).Value = "DATA DE CRIAÇÃO DO USUÁRIO";
                worksheet.Cell(currentRow, 30).Value = "PEDIDO";
                worksheet.Cell(currentRow, 31).Value = "COD REFERÊNCIA";
                worksheet.Cell(currentRow, 32).Value = "NOME MODELO - COR";
                worksheet.Cell(currentRow, 33).Value = "COD REFERENCIA LOCAL DE RETIRADA";
                worksheet.Cell(currentRow, 34).Value = "LOCAL DE RETIRADA";
                worksheet.Cell(currentRow, 35).Value = "DATA DE CRIAÇÃO DO PEDIDO";
                worksheet.Cell(currentRow, 36).Value = "ÚLTIMA ATUALIZAÇÃO DO PEDIDO";
                worksheet.Cell(currentRow, 37).Value = "STATUS";
                worksheet.Cell(currentRow, 38).Value = "DN PEDIDO";
                worksheet.Cell(currentRow, 39).Value = "USUÁRIO PEDIDO";
                foreach (var user in users)
                {
                    if (!orders.Any(p => p.UserId == user.UserId))
                    {
                        if (string.IsNullOrEmpty(user.CpfCnpj) || (user.CpfCnpj != null && user.CpfCnpj.Length > 11))
                            continue;

                        if (user.DealershipId.HasValue && user.DealershipId.Value > 0)
                            continue;

                        currentRow++;
                        AddUserColumns(customers, worksheet, currentRow, user, address, job, spouse);
                    }
                    else
                    {
                        var ordersByUser = orders.Where(p => p.UserId == user.UserId);
                        foreach (var order in ordersByUser)
                        {
                            if (order.CpfCnpj != null && order.CpfCnpj.Length > 11)
                                continue;

                            currentRow++;
                            // foreach para buscar informações por item
                            var item = orderItens.FirstOrDefault(x => x.OrderId == order.OrderId);

                            AddUserColumns(customers, worksheet, currentRow, user, address, job, spouse);
                            worksheet.Cell(currentRow, 30).Value = (order.UserIdDn.HasValue && order.UserIdDn.Value > 0) ? "SDI" + order.OrderId : "SD" + order.OrderId;
                            worksheet.Cell(currentRow, 31).Value = products.FirstOrDefault(x => x.ProductId == item.ProductId)?.PlanCode;
                            worksheet.Cell(currentRow, 32).Value = (string.IsNullOrEmpty(item?.Model)) ? item?.Model + " - " + item?.Color : products.FirstOrDefault(x => x.ProductId == item.ProductId).Model + " - " + products.FirstOrDefault(x => x.ProductId == item.ProductId).Color;
                            worksheet.Cell(currentRow, 33).Value = (order.LocationId == 0) ?  dealership.FirstOrDefault(x => x.ID == order.DealershipId)?.Number.ToString() : locations.FirstOrDefault(p => p.Id == order.LocationId)?.ReferenceCode;
                            worksheet.Cell(currentRow, 34).Value = (order.LocationId == 0) ? dealership.FirstOrDefault(x => x.ID == order.DealershipId)?.Name.ToString() : locations.FirstOrDefault(p => p.Id == order.LocationId)?.Name;
                            worksheet.Cell(currentRow, 35).Value = order.DateCreated.AddHours(-3).ToString("yyyy-MM-dd HH:mm:ss");
                            worksheet.Cell(currentRow, 36).Value = order.DateUpdated?.AddHours(-3).ToString("yyyy-MM-dd HH:mm:ss");
                            worksheet.Cell(currentRow, 37).Value = ((OrderStatusEnum)order.Status).GetDescription();
                            worksheet.Cell(currentRow, 38).Value = dealership.FirstOrDefault(x => x.ID == order.DealershipId)?.Name;
                            worksheet.Cell(currentRow, 39).Value = users.FirstOrDefault(x => x.UserId == order.UserIdDn)?.Fullname;
                        }
                    }
                }
                worksheet.Columns(1, 99).AdjustToContents();

                //ExportUsuarios(users, customers, workbook);
                //ExportOrders(users, orders, products, locations, customers, workbook);
                ExportStock(stock, workbook, products);

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "Dados.xlsx");
                }
            }

            //return View();
        }

        private static void AddUserColumns(List<Customer> customers, IXLWorksheet worksheet, int currentRow, User user, List<Address> address, List<Job> job, List<Spouse> spouse)
        {
            var customer = customers.FirstOrDefault(p => p.UserId == user.UserId);
            if (customer != null)
            {
                customer.Address = address.FirstOrDefault(p => p.CustomerId == customer.CustomerId);
                customer.Job = job.FirstOrDefault(p => p.CustomerId == customer.CustomerId);
                customer.Spouse = spouse.FirstOrDefault(p => p.CustomerId == customer.CustomerId);
            }

            worksheet.Cell(currentRow, 1).Value = user.UserId;
            worksheet.Cell(currentRow, 2).Value = user.Fullname;
            worksheet.Cell(currentRow, 3).Value = string.IsNullOrEmpty(user.CpfCnpj) ? "" : (user.CpfCnpj.Length > 11) ? Convert.ToUInt64(user.CpfCnpj).ToString(@"00\.000\.000\/0000\-00") : Convert.ToUInt64(user.CpfCnpj).ToString(@"000\.000\.000\-00");
            worksheet.Cell(currentRow, 4).Value = customer?.Rg;
            worksheet.Cell(currentRow, 6).Value = customer?.Phone;
            worksheet.Cell(currentRow, 6).Value = string.IsNullOrEmpty(customer?.CellPhone) ? "" : (customer?.CellPhone.Length == 11) ? Convert.ToUInt64(customer?.CellPhone).ToString(@"\(00\)00000\-0000") : (customer?.CellPhone == null) ? "" : Convert.ToUInt64(customer?.CellPhone).ToString(@"\(00\)0000\-0000");
            worksheet.Cell(currentRow, 7).Value = user.Email;
            worksheet.Cell(currentRow, 8).Value = customer?.AlternativeEmail;
            worksheet.Cell(currentRow, 9).Value = customer?.BirthDate.ToString("yyyy-MM-dd");
            worksheet.Cell(currentRow, 10).Value = customer?.Gender.ToString();
            worksheet.Cell(currentRow, 11).Value = customer?.CivilStatus.ToString();
            worksheet.Cell(currentRow, 12).Value = customer?.Job?.Company;
            worksheet.Cell(currentRow, 13).Value = customer?.Job?.AdmissionDate?.ToString("yyyy-MM-dd");
            worksheet.Cell(currentRow, 14).Value = customer?.Job?.Phone;
            worksheet.Cell(currentRow, 15).Value = customer?.Job?.Position;
            worksheet.Cell(currentRow, 16).Value = customer?.Job?.Salary;
            worksheet.Cell(currentRow, 17).Value = customer?.Spouse?.Name;
            worksheet.Cell(currentRow, 18).Value = customer?.Spouse?.Cpf;
            worksheet.Cell(currentRow, 19).Value = customer?.Spouse?.Salary;
            worksheet.Cell(currentRow, 20).Value = customer?.Spouse?.Phone;
            worksheet.Cell(currentRow, 21).Value = customer?.Spouse?.Email;
            worksheet.Cell(currentRow, 22).Value = customer?.Address?.ZipCode;
            worksheet.Cell(currentRow, 23).Value = customer?.Address?.Street;
            worksheet.Cell(currentRow, 24).Value = customer?.Address?.Number;
            worksheet.Cell(currentRow, 25).Value = customer?.Address?.Complement;
            worksheet.Cell(currentRow, 26).Value = customer?.Address?.District;
            worksheet.Cell(currentRow, 27).Value = customer?.Address?.City;
            worksheet.Cell(currentRow, 28).Value = customer?.Address?.State;
            worksheet.Cell(currentRow, 29).Value = user.CreateDate.AddHours(-3).ToString("yyyy-MM-dd HH:mm:ss");
        }

        private static void ExportStock(List<Stock> stock, XLWorkbook workbook, List<Product> products)
        {
            var worksheet = workbook.Worksheets.Add("Estoque");
            var currentRow = 1;
            worksheet.Cell(currentRow, 1).Value = "Cod Modelo";
            worksheet.Cell(currentRow, 2).Value = "Nome Modelo - Cor";
            worksheet.Cell(currentRow, 3).Value = "Opcional";
            worksheet.Cell(currentRow, 4).Value = "Quantidade";
            foreach (var estoque in stock)
            {
                currentRow++;
                worksheet.Cell(currentRow, 1).Value = estoque.ModelColor.Split('/').First();
                worksheet.Cell(currentRow, 2).Value = products.FirstOrDefault(p => p.ModelCode == Convert.ToInt32(estoque.ModelColor.Split('/').First())).Model + " - " + estoque.ModelColor.Split('/').ToArray()[1];
                worksheet.Cell(currentRow, 3).Value = estoque.ModelColor.Split('/').Count() > 2 ? estoque.ModelColor.Split('/').ToArray()[2] : "";
                worksheet.Cell(currentRow, 4).Value = estoque.Quantity;
            }

            worksheet.Columns(1, 99).AdjustToContents();
        }

        private static void ExportOrders(List<User> users, List<Order> orders, List<Product> products, List<Location> locations, List<Customer> customers, XLWorkbook workbook)
        {
            var worksheet = workbook.Worksheets.Add("Pedidos");
            var currentRow = 1;
            worksheet.Cell(currentRow, 1).Value = "Id do Usuário";
            worksheet.Cell(currentRow, 2).Value = "Nome do Usuário";
            worksheet.Cell(currentRow, 3).Value = "Celular";
            worksheet.Cell(currentRow, 4).Value = "E-mail";
            worksheet.Cell(currentRow, 5).Value = "Pedido";
            worksheet.Cell(currentRow, 6).Value = "Modelo - Cor";
            worksheet.Cell(currentRow, 7).Value = "Local de Retirada";
            worksheet.Cell(currentRow, 8).Value = "Data de Criação do Pedido";
            worksheet.Cell(currentRow, 9).Value = "Última Atualização do Pedido";
            worksheet.Cell(currentRow, 10).Value = "Status";
            foreach (var order in orders)
            {
                currentRow++;
                worksheet.Cell(currentRow, 1).Value = order.UserId;
                worksheet.Cell(currentRow, 2).Value = users.FirstOrDefault(p => p.UserId == order.OrderId)?.Fullname.Trim();
                worksheet.Cell(currentRow, 3).Value = customers.FirstOrDefault(p => p.UserId == order.UserId)?.CellPhone;
                worksheet.Cell(currentRow, 4).Value = users.FirstOrDefault(p => p.UserId == order.OrderId)?.Email.Trim();
                worksheet.Cell(currentRow, 5).Value = order.OrderId;
                //worksheet.Cell(currentRow, 6).Value = products.FirstOrDefault(p => p.ProductId == order.ProductId)?.Model + " - " + products.FirstOrDefault(p => p.ProductId == order.ProductId)?.Color;
                worksheet.Cell(currentRow, 7).Value = locations.FirstOrDefault(p => p.Id == order.LocationId)?.Name.Trim();
                worksheet.Cell(currentRow, 8).Value = order.DateCreated.AddHours(-3).ToString("yyyy-MM-dd HH:mm:ss");
                worksheet.Cell(currentRow, 9).Value = order.DateUpdated?.AddHours(-3).ToString("yyyy-MM-dd HH:mm:ss");
                worksheet.Cell(currentRow, 10).Value = order.Status.ToString();
            }
            worksheet.Columns(1, 99).AdjustToContents();
        }

        private static void ExportUsuarios(List<User> users, List<Customer> customers, XLWorkbook workbook)
        {
            var worksheet = workbook.Worksheets.Add("Usuários");
            var currentRow = 1;
            worksheet.Cell(currentRow, 1).Value = "Id";
            worksheet.Cell(currentRow, 2).Value = "Nome";
            worksheet.Cell(currentRow, 3).Value = "Celular";
            worksheet.Cell(currentRow, 4).Value = "E-mail";
            worksheet.Cell(currentRow, 5).Value = "Data de Criação";
            foreach (var user in users)
            {
                currentRow++;
                worksheet.Cell(currentRow, 1).Value = user.UserId;
                worksheet.Cell(currentRow, 2).Value = user.Fullname.Trim();
                worksheet.Cell(currentRow, 3).Value = customers.FirstOrDefault(p => p.UserId == user.UserId)?.CellPhone;
                worksheet.Cell(currentRow, 4).Value = user.Email;
                worksheet.Cell(currentRow, 5).Value = user.CreateDate.AddHours(-3).ToString("yyyy-MM-dd HH:mm:ss");
            }
            worksheet.Columns(1, 99).AdjustToContents();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
