﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public enum GenderEnum
    {
        /// <summary>
        /// Masculino
        /// </summary>
        [Description("Masculino")]
        Masculino = 1,

        /// <summary>
        /// Feminino
        /// </summary>
        [Description("Feminino")]
        Feminino = 2,

        /// <summary>
        /// Outros
        /// </summary>
        [Description("Outros")]
        Indefinido = 3
    }

    public enum CivilStatusEnum
    {
        /// <summary>
        /// Solteiro
        /// </summary>
        [Description("Solteiro")]
        Solteiro = 1,

        /// <summary>
        /// Casado
        /// </summary>
        [Description("Casado")]
        Casado = 2,

        /// <summary>
        /// Divorciado
        /// </summary>
        [Description("Divorciado")]
        Divorciado = 3,

        /// <summary>
        /// Viúvo
        /// </summary>
        [Description("Viúvo")]
        Viuvo = 4,

        /// <summary>
        /// União Estável
        /// </summary>
        [Description("União Estável")]
        UniaoEstavel = 5
    }

    public enum StateEnum
    {
        [Description("Acre")]
        AC = 1,

        [Description("Alagoas")]
        AL = 2,

        [Description("Amapá")]
        AP = 3,

        [Description("Amazonas")]
        AM = 4,

        [Description("Bahia")]
        BA = 5,

        [Description("Ceará")]
        CE = 6,

        [Description("Distrito Federal")]
        DF = 7,

        [Description("Espírito Santo")]
        ES = 8,

        [Description("Goiás")]
        GO = 9,

        [Description("Maranhão")]
        MA = 10,

        [Description("Mato Grosso")]
        MT = 11,

        [Description("Mato Grosso do Sul")]
        MS = 12,

        [Description("Minas Gerais")]
        MG = 13,

        [Description("Paraná")]
        PR = 14,

        [Description("Paraíba")]
        PB = 15,

        [Description("Pará")]
        PA = 16,

        [Description("Pernambuco")]
        PE = 17,

        [Description("Piauí")]
        PI = 18,

        [Description("Rio de Janeiro")]
        RJ = 19,

        [Description("Rio Grande do Norte")]
        RN = 20,

        [Description("Rio Grande do Sul")]
        RS = 21,

        [Description("Rondônia")]
        RO = 22,

        [Description("Roraima")]
        RR = 23,

        [Description("Santa Catarina")]
        SC = 24,

        [Description("Sergipe")]
        SE = 25,

        [Description("São Paulo")]
        SP = 26,

        [Description("Tocantins")]
        TO = 27
    }

    public class Customer
    {
        public long CustomerId { get; set; }

        public long UserId { get; set; }

        public DateTime BirthDate { get; set; }

        public string AlternativeEmail { get; set; }

        public GenderEnum Gender { get; set; }

        public string Rg { get; set; }

        public string Phone { get; set; }

        public string CellPhone { get; set; }

        public CivilStatusEnum CivilStatus { get; set; }

        public Address Address { get; set; }

        public Spouse Spouse { get; set; }

        public Job Job { get; set; }


        public int? IdUserCreated { get; set; }
        public int? IdUserUpdated { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
    }

    public class Address
    {

        public long AddressId { get; set; }

        /// <summary>
        /// CEP
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Nome da rua
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Número
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Bairro
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// Cidade
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Estado
        /// </summary>
        public StateEnum State { get; set; }

        /// <summary>
        /// Complemento
        /// </summary>
        public string Complement { get; set; }

        public long? CustomerId { get; set; }
        public Customer Customer { get; set; }

        public long? CompanyId { get; set; }
        public Company Company { get; set; }


        public int? IdUserCreated { get; set; }
        public int? IdUserUpdated { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
    }

    public class Job 
    {
        public long JobId { get; set; }

        /// <summary>
        /// Nome empresa
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Data de admissão
        /// </summary>
        public DateTime? AdmissionDate { get; set; }

        /// <summary>
        /// Cargo
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// Relação funcional
        /// </summary>
        public string FunctionalRelationship { get; set; }

        /// <summary>
        /// Telefone
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Salário
        /// </summary>
        public decimal Salary { get; set; }

        public long CustomerId { get; set; }


        public int? IdUserCreated { get; set; }
        public int? IdUserUpdated { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }

    }

    public class Spouse 
    {
        public long SpouseId { get; set; }

        /// <summary>
        /// Nome Cônjuge
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// CPF cônjuge
        /// </summary>
        public string Cpf { get; set; }

        /// <summary>
        /// Cônjuge compõe renda?
        /// </summary>
        public bool SpouseMakesUpIncome { get; set; }

        /// <summary>
        /// E-mail cônjuge
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Telefone cônjuge
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Salário cônjuge
        /// </summary>
        public decimal Salary { get; set; }

        public long CustomerId { get; set; }


        public int? IdUserCreated { get; set; }
        public int? IdUserUpdated { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}
