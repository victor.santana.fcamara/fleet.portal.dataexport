﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class DealershipContext : DbContext
    {
        public DbSet<Dealership> Dealership { get; set; }

        public DealershipContext(DbContextOptions<DealershipContext> options) :
            base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Dealership>().HasKey(c => c.ID);
        }
    }
}
