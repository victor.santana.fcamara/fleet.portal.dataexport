﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class UserContext : DbContext
    {
        public DbSet<User> User { get; set; }

        public UserContext(DbContextOptions<UserContext> options) :
            base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasKey(c => c.UserId);
        }
    }
   
}
