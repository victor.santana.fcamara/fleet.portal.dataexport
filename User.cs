﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class User
    {
        public long UserId { get; set; }

        public string Fullname { get; set; }

        public string Email { get; set; }

        public string CpfCnpj { get; set; }

        public string Password { get; set; }

        public bool Deleted { get; set; }

        public DateTime CreateDate { get; set; }

        public Guid? ConfirmationEmailToken { get; set; }

        public long? DealershipId { get; set; }

        public bool EmailConfirmed { get; set; }

        public Guid? PasswordResetToken { get; set; }

        public DateTime? PasswordResetExpiration { get; set; }

        public bool Inactive { get; set; }

        public Guid KongConsumerId { get; set; }
    }
}
