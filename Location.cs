﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fleet.portal.dataexport
{
    public class Location
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string ReferenceCode { get; set; }
    }
}
